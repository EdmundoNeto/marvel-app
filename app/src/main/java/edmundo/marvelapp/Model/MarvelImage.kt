package edmundo.marvelapp.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MarvelImage (
    var path: String?,
    var extension: String?
): Parcelable
