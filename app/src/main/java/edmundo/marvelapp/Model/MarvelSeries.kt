package edmundo.marvelapp.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MarvelSeries (
    var resourceURI: String?,
    var name: String?
    ): Parcelable
