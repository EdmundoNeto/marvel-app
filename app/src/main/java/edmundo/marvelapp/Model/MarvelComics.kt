package edmundo.marvelapp.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class  MarvelComics (
    var resourceURI: String?,
    var name: String?
): Parcelable
