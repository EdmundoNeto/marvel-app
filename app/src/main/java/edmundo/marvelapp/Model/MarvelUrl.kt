package edmundo.marvelapp.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MarvelUrl(
    val type: String?,
    val url: String?
    ): Parcelable
