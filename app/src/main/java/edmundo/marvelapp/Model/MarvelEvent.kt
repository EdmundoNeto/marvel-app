package edmundo.marvelapp.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MarvelEvent (
    var resourceURI: String?,
    var name: String?
): Parcelable
