package edmundo.marvelapp.Model;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MarvelCharacters implements Serializable {

    private int offset;
    private int limit;
    private int total;
    private int count;
    private List<MarvelCharacterDetail> results = new ArrayList<>();

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<MarvelCharacterDetail> getResults() {
        return results;
    }

    public void setResults(List<MarvelCharacterDetail> results) {
        this.results = results;
    }

}
