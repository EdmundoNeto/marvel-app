package edmundo.marvelapp.Model

import android.os.Parcelable
import android.support.v7.util.DiffUtil
import kotlinx.android.parcel.Parcelize

import java.io.Serializable

@Parcelize
data class MarvelCharacterDetail(
    val id: String?,
    val name: String?,
    val description: String?,
    val modified: String?,
    val resourceUri: String?,
    val urls: List<MarvelUrl>?,
    val thumbnail: MarvelImage?,
    val comics: MarvelResources<MarvelComics>?,
    val stories: MarvelResources<MarvelStories>?,
    val events: MarvelResources<MarvelEvent>?,
    val series: MarvelResources<MarvelSeries>?
) : Parcelable, Serializable