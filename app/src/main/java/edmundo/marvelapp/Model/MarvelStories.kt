package edmundo.marvelapp.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class  MarvelStories (
    var resourceURI: String?,
    var name: String? ,
    var type: String?
): Parcelable
