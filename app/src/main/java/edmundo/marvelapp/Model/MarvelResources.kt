package edmundo.marvelapp.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class MarvelResources<T> (
    val available: Int,
    val returned: Int,
    val collectionUri: String?,
    val items: @RawValue List<T>?
): Parcelable
