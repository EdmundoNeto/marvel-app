package edmundo.marvelapp.Model

import java.io.Serializable
import java.util.ArrayList

class Marvel<T> : Serializable {
    var code: Int = 0
    var status: String? = null
    var copyright: String? = null
    var attributionText: String? = null
    var attributionHTML: String? = null
    var etag: String? = null
    var data: T? = null
}
