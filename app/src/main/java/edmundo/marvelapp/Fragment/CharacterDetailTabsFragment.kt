package edmundo.marvelapp.Fragment

import android.content.Context
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import edmundo.marvelapp.Model.MarvelCharacterDetail
import edmundo.marvelapp.Model.MarvelComics
import edmundo.marvelapp.R
import edmundo.marvelapp.Model.MarvelSeries
import edmundo.marvelapp.Model.MarvelStories


class CharacterDetailTabsFragment : Fragment() {

    companion object {
        fun newInstance(marvelCharacterDetail: MarvelCharacterDetail): CharacterDetailTabsFragment {
            val args = Bundle()
            val fragment =  CharacterDetailTabsFragment()
            args.putSerializable("marvelCharacterDetail", marvelCharacterDetail)
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var viewPager: ViewPager
    private lateinit var tabsAdapter: TabsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.content_character_detail, container, false)

        viewPager = view.findViewById(R.id.vpCharacterDetail)
        setupView(view)
        return view
    }

    private fun setupView(view: View){
        setupViewPager(viewPager)
        setupTabLayout(view, viewPager)
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val marvelCharacterDetail = arguments?.getSerializable("marvelCharacterDetail") as MarvelCharacterDetail
        tabsAdapter = TabsAdapter(childFragmentManager, context!!)

        tabsAdapter.addFragment(CharacterTabFragment.newInstance(marvelCharacterDetail), "Character")
        tabsAdapter.addFragment(CharacterTabComicsFragment.newInstance(marvelCharacterDetail.comics?.items as ArrayList<MarvelComics?> ), "Comics")
        tabsAdapter.addFragment(CharacterTabSeriesFragment.newInstance(marvelCharacterDetail.series?.items as ArrayList<MarvelSeries?> ), "Series")
        tabsAdapter.addFragment(CharacterTabStoriesFragment.newInstance(marvelCharacterDetail.stories?.items as ArrayList<MarvelStories?> ), "Stories")

        viewPager.adapter = tabsAdapter
    }

    private fun setupTabLayout(view: View, viewPager: ViewPager) {
        val tabLayout = view.findViewById<TabLayout>(R.id.tlCharacterDetail)
        tabLayout.setupWithViewPager(viewPager)

        tabLayout.setSelectedTabIndicatorColor(resources.getColor(R.color.colorAccent))
        tabLayout.setTabTextColors(resources.getColor(R.color.colorWhite), resources.getColor(R.color.colorWhite))

        tabLayout.setupWithViewPager(viewPager)

    }

    class TabsAdapter(fm: FragmentManager, val context: Context) : FragmentPagerAdapter(fm) {
        private val mFragments: ArrayList<Fragment> = ArrayList()
        private val mFragmentTitles: ArrayList<String> = ArrayList()


        fun addFragment(fragment: Fragment, title: String) {
            mFragments.add(fragment)
            mFragmentTitles.add(title)
        }

        override fun getItem(position: Int): Fragment {
            return mFragments[position]
        }

        override fun getCount(): Int {
            return mFragments.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitles[position]
        }

    }


}