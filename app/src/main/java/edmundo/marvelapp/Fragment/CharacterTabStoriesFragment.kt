package edmundo.marvelapp.Fragment

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.databinding.library.baseAdapters.BR
import dagger.android.support.AndroidSupportInjection
import edmundo.marvelapp.Model.MarvelStories
import edmundo.marvelapp.R
import edmundo.marvelapp.adapter.StoriesListAdapter
import edmundo.marvelapp.databinding.TabStoriesCharacterDetailBinding
import edmundo.marvelapp.viewModel.CharacterDetailViewModel
import edmundo.marvelapp.viewModel.factory.CharacterDetailViewModelFactory
import javax.inject.Inject

class CharacterTabStoriesFragment: Fragment() {

    companion object {
        fun newInstance(stories: ArrayList<MarvelStories?>): CharacterTabStoriesFragment {
            val args = Bundle()
            val fragment =  CharacterTabStoriesFragment()
            args.putSerializable("stories", stories)
            fragment.arguments = args
            return fragment
        }
    }

    @Inject
    lateinit var characterDetailViewModelFactory: CharacterDetailViewModelFactory
    @Inject
    lateinit var storiesListAdapter: StoriesListAdapter

    private lateinit var characterDetailViewModel: CharacterDetailViewModel

    private lateinit var binding: TabStoriesCharacterDetailBinding
    private lateinit var mLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.tab_stories_character_detail, container, false)
        characterDetailViewModel = ViewModelProviders.of(activity!!, characterDetailViewModelFactory).get(CharacterDetailViewModel::class.java)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.setVariable(BR.characterDetailViewModelStories, characterDetailViewModel)
        setupAdapter()
    }

    private fun setupAdapter(){
        val series = arguments?.getSerializable("stories") as ArrayList<MarvelStories?>
        storiesListAdapter.addItems(series as List<MarvelStories>)
        mLayoutManager = LinearLayoutManager(context)
        mLayoutManager.orientation = LinearLayoutManager.VERTICAL
        binding.rvStories.layoutManager = mLayoutManager
        binding.rvStories.adapter = storiesListAdapter
    }

}