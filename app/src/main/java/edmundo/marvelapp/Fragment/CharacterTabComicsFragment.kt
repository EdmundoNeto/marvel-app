package edmundo.marvelapp.Fragment

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.databinding.library.baseAdapters.BR
import dagger.android.support.AndroidSupportInjection
import edmundo.marvelapp.Model.MarvelComics
import edmundo.marvelapp.R
import edmundo.marvelapp.adapter.ComicsListAdapter
import edmundo.marvelapp.databinding.TabComicsCharacterDetailBinding
import edmundo.marvelapp.viewModel.CharacterDetailViewModel
import edmundo.marvelapp.viewModel.factory.CharacterDetailViewModelFactory
import javax.inject.Inject

class CharacterTabComicsFragment : Fragment() {

    companion object {
        fun newInstance(comics: ArrayList<MarvelComics?>): CharacterTabComicsFragment {
            val args = Bundle()
            val fragment =  CharacterTabComicsFragment()
            args.putSerializable("comics", comics)
            fragment.arguments = args
            return fragment
        }
    }

    @Inject
    lateinit var characterDetailViewModelFactory: CharacterDetailViewModelFactory
    @Inject
    lateinit var comicsListAdapter: ComicsListAdapter

    private lateinit var characterDetailViewModel: CharacterDetailViewModel

    private lateinit var binding: TabComicsCharacterDetailBinding
    private lateinit var mLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.tab_comics_character_detail, container, false)
        characterDetailViewModel = ViewModelProviders.of(activity!!, characterDetailViewModelFactory).get(CharacterDetailViewModel::class.java)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.setVariable(BR.characterDetailViewModelComics, characterDetailViewModel)
        setupAdapter()
    }

    private fun setupAdapter(){
        val comics = arguments?.getSerializable("comics") as ArrayList<MarvelComics?>
        comicsListAdapter.addItems(comics as List<MarvelComics>)
        mLayoutManager = LinearLayoutManager(context)
        mLayoutManager.orientation = LinearLayoutManager.VERTICAL
        binding.rvComics.layoutManager = mLayoutManager
        binding.rvComics.adapter = comicsListAdapter
    }

}