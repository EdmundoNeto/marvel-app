package edmundo.marvelapp.Fragment

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.databinding.library.baseAdapters.BR
import edmundo.marvelapp.Model.MarvelCharacterDetail
import edmundo.marvelapp.R
import edmundo.marvelapp.databinding.TabCharacterDetailBinding

class CharacterTabFragment : Fragment() {

    companion object {
        fun newInstance(character: MarvelCharacterDetail?): CharacterTabFragment {
            val args = Bundle()
            val fragment =  CharacterTabFragment()
            args.putSerializable("character", character)
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var binding: TabCharacterDetailBinding
    private lateinit var marvelCharacterDetail: MarvelCharacterDetail

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.tab_character_detail, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        marvelCharacterDetail = arguments?.getSerializable("character") as MarvelCharacterDetail
        binding.setVariable(BR.marvelCharacterDetail, marvelCharacterDetail)
        binding.executePendingBindings()
    }

}