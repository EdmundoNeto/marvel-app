package edmundo.marvelapp.Fragment

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.databinding.library.baseAdapters.BR
import dagger.android.support.AndroidSupportInjection
import edmundo.marvelapp.Model.MarvelSeries
import edmundo.marvelapp.R
import edmundo.marvelapp.adapter.SeriesListAdapter
import edmundo.marvelapp.databinding.TabSeriesCharacterDetailBinding
import edmundo.marvelapp.viewModel.CharacterDetailViewModel
import edmundo.marvelapp.viewModel.factory.CharacterDetailViewModelFactory
import javax.inject.Inject

class CharacterTabSeriesFragment: Fragment() {

    companion object {
        fun newInstance(series: ArrayList<MarvelSeries?>): CharacterTabSeriesFragment {
            val args = Bundle()
            val fragment =  CharacterTabSeriesFragment()
            args.putSerializable("series", series)
            fragment.arguments = args
            return fragment
        }
    }

    @Inject
    lateinit var characterDetailViewModelFactory: CharacterDetailViewModelFactory
    @Inject
    lateinit var seriesListAdapter: SeriesListAdapter

    private lateinit var characterDetailViewModel: CharacterDetailViewModel

    private lateinit var binding: TabSeriesCharacterDetailBinding
    private lateinit var mLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.tab_series_character_detail, container, false)
        characterDetailViewModel = ViewModelProviders.of(activity!!, characterDetailViewModelFactory).get(CharacterDetailViewModel::class.java)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.setVariable(BR.characterDetailViewModelSeries, characterDetailViewModel)
        setupAdapter()
    }

    private fun setupAdapter(){
        val series = arguments?.getSerializable("series") as ArrayList<MarvelSeries?>
        seriesListAdapter.addItems(series as List<MarvelSeries>)
        mLayoutManager = LinearLayoutManager(context)
        mLayoutManager.orientation = LinearLayoutManager.VERTICAL
        binding.rvSeries.layoutManager = mLayoutManager
        binding.rvSeries.adapter = seriesListAdapter
    }

}