package edmundo.marvelapp.Activity

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.android.databinding.library.baseAdapters.BR
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import edmundo.marvelapp.Fragment.CharacterDetailTabsFragment
import edmundo.marvelapp.Model.MarvelCharacterDetail
import edmundo.marvelapp.R
import edmundo.marvelapp.Utils.MarvelUtils.*
import edmundo.marvelapp.databinding.ActivityCharacterDetailBinding
import edmundo.marvelapp.viewModel.CharacterDetailViewModel
import edmundo.marvelapp.viewModel.factory.CharacterDetailViewModelFactory
import kotlinx.android.synthetic.main.activity_character_detail.*
import javax.inject.Inject

class CharacterDetailActivity : AppCompatActivity() {


    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>
    @Inject
    lateinit var characterDetailViewModelFactory: CharacterDetailViewModelFactory

    private lateinit var characterDetailViewModel: CharacterDetailViewModel

    private lateinit var binding: ActivityCharacterDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        characterDetailViewModel = ViewModelProviders.of(this, characterDetailViewModelFactory).get(
                CharacterDetailViewModel::class.java)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_character_detail)
        binding.characterDetailViewModel = characterDetailViewModel

        setupViewModel(intent.extras["characterId"] as String)

    }

    private fun setupViewModel(characterId: String){

        characterDetailViewModel.loadMarvelCharacterDetail(TIMESTAMP, characterId, PUBLIC_KEY, getHash(TIMESTAMP, PRIVATE_KEY, PUBLIC_KEY))

        characterDetailViewModel.marvelCharacterDetailResult().observe(this,
                Observer<MarvelCharacterDetail> {
                    if (it != null) {
                        setViewModelLists(it)
                        setBindingVariable(it)
                        openTabsFragment(it)
                    }
                })

        characterDetailViewModel.marvelCharacterDetailError().observe(this, Observer<String> {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })


        characterDetailViewModel.marvelRepositoryLoader().observe(this, Observer<Boolean> {
            setItensVisibility(it!!)
        })

    }

    private fun setViewModelLists(marvelCharacterDetail: MarvelCharacterDetail){
        characterDetailViewModel.addComicsToList(marvelCharacterDetail.comics!!.items!!)
        characterDetailViewModel.addSeriesToList(marvelCharacterDetail.series!!.items!!)
        characterDetailViewModel.addStoriesToList(marvelCharacterDetail.stories!!.items!!)
    }

    private fun setBindingVariable(marvelCharacterDetail: MarvelCharacterDetail) {
        binding.setVariable(BR.marvelCharacterDetailActivitty, marvelCharacterDetail)
        binding.executePendingBindings()
    }

    private fun openTabsFragment(marvelCharacterDetail: MarvelCharacterDetail) {
        val fragmentManager = supportFragmentManager

        fragmentManager.beginTransaction()
                .replace(R.id.containerCharacterDetail, CharacterDetailTabsFragment.newInstance(marvelCharacterDetail), "marvelCharacterDetail")
                .commit()
    }

    private fun setItensVisibility(visible: Boolean){
        pbCharacterDetail.visibility = if (!visible) View.VISIBLE else View.GONE
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }
}
