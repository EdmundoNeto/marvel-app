package edmundo.marvelapp.Activity

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.View
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import edmundo.marvelapp.R
import edmundo.marvelapp.adapter.CharacterListAdapter
import edmundo.marvelapp.databinding.ActivityMarvelListBinding
import edmundo.marvelapp.viewModel.CharacterListViewModel
import edmundo.marvelapp.viewModel.factory.CharacterListViewModelFactory
import kotlinx.android.synthetic.main.activity_marvel_list.*
import javax.inject.Inject
import edmundo.marvelapp.Utils.State
import io.reactivex.disposables.Disposable


class MainActivity : AppCompatActivity(), HasActivityInjector {

    @Inject
    lateinit var activityDispatchingAndroidInjector:  DispatchingAndroidInjector<Activity>
    @Inject
    lateinit var mCharacterListAdapter: CharacterListAdapter
    @Inject
    lateinit var characterListViewModelFactory: CharacterListViewModelFactory

    private lateinit var characterListViewModel: CharacterListViewModel
    private lateinit var mLayoutManager: GridLayoutManager
    private lateinit var binding: ActivityMarvelListBinding

    private var subscribeMarvelCharacterClickEvent:  Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        characterListViewModel = ViewModelProviders.of(this, characterListViewModelFactory).get(
                CharacterListViewModel::class.java)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_marvel_list)
        binding.characterListViewModel = characterListViewModel

        initAdapter()
        initState()
        observeAdapterClick()
    }

    private fun observeAdapterClick(){
        subscribeMarvelCharacterClickEvent = mCharacterListAdapter.mMarvelCharacterClickEvent.subscribe {
            startActivity(Intent(this, CharacterDetailActivity::class.java).putExtra("characterId", it.id))
        }
    }

    private fun initAdapter() {
        mLayoutManager = GridLayoutManager(this, 2)
        mLayoutManager.orientation = GridLayoutManager.VERTICAL
        recycler_view.layoutManager = mLayoutManager
        recycler_view.adapter = mCharacterListAdapter
        characterListViewModel.charactersList.observe(this, Observer {
            mCharacterListAdapter.submitList(it)
        })
    }

    private fun initState() {
        characterListViewModel.getState().observe(this, Observer { state ->
            item_progress_bar.visibility = if (characterListViewModel.listIsEmpty() && state == State.LOADING) View.VISIBLE else View.GONE
            recycler_view.visibility = if (state == State.DONE) View.VISIBLE else View.GONE
            if (!characterListViewModel.listIsEmpty()) {
                mCharacterListAdapter.setState(state ?: State.DONE)
            }
        })
    }


    override fun activityInjector(): AndroidInjector<Activity> {
        return activityDispatchingAndroidInjector
    }
}