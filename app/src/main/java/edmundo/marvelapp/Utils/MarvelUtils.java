package edmundo.marvelapp.Utils;

import java.security.MessageDigest;

public class MarvelUtils {

    public static String PUBLIC_KEY = "db2d58420be935ff0816215c3dc318bc";
    public static String PRIVATE_KEY = "d401f43388a80f25492631dc49e26201a613e3d3";
    public static String TIMESTAMP = "1";
    public static int LIMIT = 20;



    public static String getHash(String TIMESTAMP, String PRIVATE_KEY, String PUBLIC_KEY){
        return getMd5Key(TIMESTAMP + PRIVATE_KEY + PUBLIC_KEY);
    }

    public static String getMd5Key(String password) {
        try {
            MessageDigest md5Encoder = MessageDigest.getInstance("MD5");
            byte[] md5Bytes = md5Encoder.digest(password.getBytes());

            StringBuilder md5 = new StringBuilder();
            for (int i = 0; i < md5Bytes.length; ++i) {
                md5.append(Integer.toHexString((md5Bytes[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return md5.toString();
        } catch (Exception e) {
        }
        return "";
    }
}
