package edmundo.marvelapp.Utils

enum class State {
    DONE, LOADING, ERROR
}