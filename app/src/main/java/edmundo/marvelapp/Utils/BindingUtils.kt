package edmundo.marvelapp.Utils

import android.databinding.BindingAdapter
import android.databinding.BindingMethod
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.widget.ImageView
import com.squareup.picasso.Picasso
import edmundo.marvelapp.Model.*
import edmundo.marvelapp.R
import edmundo.marvelapp.adapter.*

object BindingUtils {

    @JvmStatic
    @BindingAdapter("bind:imageUrl")
    fun loadImage(view: ImageView, imageUrl: MarvelImage?) {
        if (imageUrl != null) {
            Picasso.get()
                    .load(imageUrl.path + "." + imageUrl.extension)
                    .placeholder(R.drawable.marvel_logo)
                    .fit()
                    .into(view)

        }

    }

    @JvmStatic
    @BindingAdapter("adapterCharacterList")
    fun adapterCharacterList(recyclerView: RecyclerView, characterList: List<MarvelCharacterDetail>) {
        val adapter = recyclerView.adapter as CharacterListAdapter?
        if (adapter != null) {
            adapter.clearItems()
            adapter.addItems(characterList)
        }
    }

    @JvmStatic
    @BindingAdapter("adapterComicsList")
    fun adapterComicsList(recyclerView: RecyclerView, comics: List<MarvelComics>) {
        val adapter = recyclerView.adapter as ComicsListAdapter?
        if (adapter != null) {
            adapter.clearItems()
            adapter.addItems(comics)
        }
    }

    @JvmStatic
    @BindingAdapter("adapterSeriesList")
    fun adapterSeriesList(recyclerView: RecyclerView, series: List<MarvelSeries>) {
        val adapter = recyclerView.adapter as SeriesListAdapter?
        if (adapter != null) {
            adapter.clearItems()
            adapter.addItems(series)
        }
    }
    @JvmStatic
    @BindingAdapter("adapterStoriesList")
    fun adapterStoriesList(recyclerView: RecyclerView, stories: List<MarvelStories>) {
        val adapter = recyclerView.adapter as StoriesListAdapter?
        if (adapter != null) {
            adapter.clearItems()
            adapter.addItems(stories)
        }
    }

}