package edmundo.marvelapp.ApiService

import edmundo.marvelapp.Model.Marvel
import edmundo.marvelapp.Model.MarvelCharacters
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MarvelClient {

    @GET("/v1/public/characters")
    fun getCharacters(
            @Query("ts") timeStamp: String,
            @Query("offset") limit: Int,
            @Query("apikey") PUBLIC_KEY: String,
            @Query("hash") hash: String
    ): Call<Marvel<MarvelCharacters>>

    @GET("/v1/public/characters/{characterId}")
    fun getCharacterDetail(
            @Path("characterId") characterId: String,
            @Query("ts") timeStamp: String,
            @Query("apikey") PUBLIC_KEY: String,
            @Query("hash") hash: String
    ): Call<Marvel<MarvelCharacters>>


}
