package edmundo.marvelapp.ApiService

import android.arch.paging.PageKeyedDataSource
import com.google.gson.Gson
import com.google.gson.JsonArray
import edmundo.marvelapp.Model.Marvel
import edmundo.marvelapp.Model.MarvelCharacterDetail
import edmundo.marvelapp.Model.MarvelCharacters
import io.reactivex.Observable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class MarvelRepository @Inject constructor(private val marvelClient: MarvelClient) {

    fun listMarvelCharacters(timeStamp: String, limit: Int, PUBLIC_KEY: String, hash: String):
            Observable<Marvel<MarvelCharacters>> {

        return Observable.create {

            val emitter = it

            marvelClient.getCharacters(
                    timeStamp, limit, PUBLIC_KEY, hash
            ).enqueue(object : Callback<Marvel<MarvelCharacters>> {
                override fun onResponse(call: Call<Marvel<MarvelCharacters>>, response: Response<Marvel<MarvelCharacters>>) {
                    if (!emitter.isDisposed){
                        emitter.onNext(response.body()!!)
                    }
                }
                override fun onFailure(call: Call<Marvel<MarvelCharacters>>, t: Throwable) {
                    emitter.onError(t)
                }

            })
        }
    }

    fun getCharacterDetail(timeStamp: String, characterId: String, PUBLIC_KEY: String, hash: String):
            Observable<MarvelCharacterDetail> {

        return Observable.create {

            val emitter = it

            marvelClient.getCharacterDetail(characterId,
                    timeStamp, PUBLIC_KEY, hash
            ).enqueue(object : Callback<Marvel<MarvelCharacters>> {
                override fun onResponse(call: Call<Marvel<MarvelCharacters>>, response: Response<Marvel<MarvelCharacters>>) {
                    if (!emitter.isDisposed){
                        emitter.onNext(response.body()!!.data!!.results[0])
                    }
                }
                override fun onFailure(call: Call<Marvel<MarvelCharacters>>, t: Throwable) {
                    emitter.onError(t)
                }

            })
        }
    }

}