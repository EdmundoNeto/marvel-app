package edmundo.marvelapp.di.Component

import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import edmundo.marvelapp.MarvelApplication
import edmundo.marvelapp.di.Module.ActivitiesProvider
import edmundo.marvelapp.di.Module.AppModule
import edmundo.marvelapp.di.Module.NetworkModule
import edmundo.marvelapp.di.Module.ViewModelModule
import javax.inject.Singleton


@Singleton
@Component(
        modules = arrayOf(AndroidSupportInjectionModule::class, AppModule::class,
                NetworkModule::class, ViewModelModule::class, ActivitiesProvider::class)
)
interface AppComponent {

    fun inject(app: MarvelApplication)
}