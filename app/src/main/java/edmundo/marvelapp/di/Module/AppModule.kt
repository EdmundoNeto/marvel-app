package edmundo.marvelapp.di.Module

import android.app.Application
import android.arch.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import edmundo.marvelapp.viewModel.factory.CharacterListViewModelFactory
import javax.inject.Singleton

@Module
class AppModule(val app: Application) {

    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideCharacterListViewModelFactory(
            factory: CharacterListViewModelFactory): ViewModelProvider.Factory = factory

}