package edmundo.marvelapp.di.Module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import edmundo.marvelapp.Activity.CharacterDetailActivity
import edmundo.marvelapp.Activity.MainActivity
import edmundo.marvelapp.Fragment.CharacterTabComicsFragment
import edmundo.marvelapp.Fragment.CharacterTabSeriesFragment
import edmundo.marvelapp.Fragment.CharacterTabStoriesFragment

@Module
abstract class ActivitiesProvider {
    @ContributesAndroidInjector
    abstract fun MainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun CharacterDetailActivity(): CharacterDetailActivity

    @ContributesAndroidInjector(modules = arrayOf(ViewModelModule::class))
    abstract fun provideCharacterTabComicsFragmentFactory(): CharacterTabComicsFragment

    @ContributesAndroidInjector(modules = arrayOf(ViewModelModule::class))
    abstract fun provideCharacterTabSeriesFragmentFactory(): CharacterTabSeriesFragment

    @ContributesAndroidInjector(modules = arrayOf(ViewModelModule::class))
    abstract fun provideCharacterTabStoriesFragmentFactory(): CharacterTabStoriesFragment

}