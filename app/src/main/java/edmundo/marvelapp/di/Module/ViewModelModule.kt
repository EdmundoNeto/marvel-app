package edmundo.marvelapp.di.Module

import android.app.Application
import android.arch.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import edmundo.marvelapp.ApiService.MarvelRepository
import edmundo.marvelapp.Model.MarvelCharacterDetail
import edmundo.marvelapp.adapter.*
import edmundo.marvelapp.viewModel.CharacterDetailViewModel
import edmundo.marvelapp.viewModel.CharacterListViewModel
import edmundo.marvelapp.viewModel.factory.CharacterDetailViewModelFactory
import edmundo.marvelapp.viewModel.factory.CharacterListViewModelFactory
import java.util.ArrayList

@Module
class ViewModelModule {

    @Provides
    fun characterListViewModel(marvelRepository: MarvelRepository): CharacterListViewModel {
        return CharacterListViewModel(marvelRepository)
    }

    @Provides
    fun provideCharacterListViewModel(characterListViewModel: CharacterListViewModel): ViewModelProvider.Factory {
        return CharacterListViewModelFactory(characterListViewModel)
    }

    @Provides
    fun characterDetailViewModel(marvelRepository: MarvelRepository): CharacterDetailViewModel {
        return CharacterDetailViewModel(marvelRepository)
    }

    @Provides
    fun providecharacterDetailViewModel(characterDetailViewModel: CharacterDetailViewModel): ViewModelProvider.Factory {
        return CharacterDetailViewModelFactory(characterDetailViewModel)
    }

    @Provides
    fun provideCharacterListAdapter(): CharacterListAdapter {
        return CharacterListAdapter(ArrayList())
    }

    @Provides
    fun provideComicsListAdapter(): ComicsListAdapter {
        return ComicsListAdapter(ArrayList())
    }

    @Provides
    fun provideSeriesListAdapter(): SeriesListAdapter {
        return SeriesListAdapter(ArrayList())
    }

    @Provides
    fun provideStorieListAdapter(): StoriesListAdapter {
        return StoriesListAdapter(ArrayList())
    }


}