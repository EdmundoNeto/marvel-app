package edmundo.marvelapp.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.android.databinding.library.baseAdapters.BR
import edmundo.marvelapp.Model.MarvelSeries
import edmundo.marvelapp.databinding.TabSeriesCharacterDetailItemBinding
import javax.inject.Inject

class SeriesListAdapter @Inject constructor(private var marvelSeries: MutableList<MarvelSeries>): RecyclerView.Adapter<SeriesListAdapter.MarvelSeriesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): MarvelSeriesViewHolder {
        val marvelSeriesBinding = TabSeriesCharacterDetailItemBinding.inflate(LayoutInflater.from(parent.context),
                parent, false)
        return MarvelSeriesViewHolder(marvelSeriesBinding)
    }

    override fun getItemCount(): Int {
        return marvelSeries.size
    }

    override fun onBindViewHolder(holder: MarvelSeriesViewHolder, position: Int) {
        (holder as? MarvelSeriesViewHolder)?.bind(marvelSeries[position])
    }


    fun addItems(comics: List<MarvelSeries>) {
        marvelSeries.addAll(comics)
        notifyDataSetChanged()
    }

    fun clearItems() {
        marvelSeries.clear()
    }

    class MarvelSeriesViewHolder constructor(private val mBinding: TabSeriesCharacterDetailItemBinding)
        : RecyclerView.ViewHolder(mBinding.root) {


        fun bind(marvelSeries: MarvelSeries) {
            mBinding.setVariable(BR.marvelSeries, marvelSeries)
            mBinding.executePendingBindings()
        }

    }
}