package edmundo.marvelapp.adapter

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PageKeyedDataSource
import edmundo.marvelapp.ApiService.MarvelRepository
import edmundo.marvelapp.Model.MarvelCharacterDetail
import edmundo.marvelapp.Utils.MarvelUtils.*
import edmundo.marvelapp.Utils.State
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import io.reactivex.schedulers.Schedulers

class CharacterDatasource(private val marvelRepository: MarvelRepository,
                          private val compositeDisposable: CompositeDisposable): PageKeyedDataSource<Int, MarvelCharacterDetail>() {

    var state: MutableLiveData<State> = MutableLiveData()
    private var retryCompletable: Completable? = null

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, MarvelCharacterDetail>) {
        updateState(State.LOADING)
        compositeDisposable.add(
                marvelRepository.listMarvelCharacters(TIMESTAMP, 0 * params.requestedLoadSize, PUBLIC_KEY, getHash(TIMESTAMP, PRIVATE_KEY, PUBLIC_KEY))
                        .subscribe(
                                { response ->
                                    updateState(State.DONE)
                                    callback.onResult(response.data!!.results,
                                            null,
                                            2
                                    )
                                },
                                {
                                    updateState(State.ERROR)
                                    setRetry(Action { loadInitial(params, callback) })
                                }
                        )
        )

    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, MarvelCharacterDetail>) {
        updateState(State.LOADING)
        compositeDisposable.add(
                marvelRepository.listMarvelCharacters(TIMESTAMP, params.key * params.requestedLoadSize, PUBLIC_KEY, getHash(TIMESTAMP, PRIVATE_KEY, PUBLIC_KEY))
                        .subscribe(
                                { response ->
                                    updateState(State.DONE)
                                    callback.onResult(response.data!!.results,
                                            params.key + 1
                                    )
                                },
                                {
                                    updateState(State.ERROR)
                                    setRetry(Action { loadAfter(params, callback) })
                                }
                        )
        )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, MarvelCharacterDetail>) {

    }

    private fun updateState(state: State) {
        this.state.postValue(state)
    }

    fun retry() {
        if (retryCompletable != null) {
            compositeDisposable.add(retryCompletable!!
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe())
        }
    }

    private fun setRetry(action: Action?) {
        retryCompletable = if (action == null) null else Completable.fromAction(action)
    }
}