package edmundo.marvelapp.adapter

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import edmundo.marvelapp.ApiService.MarvelRepository
import edmundo.marvelapp.Model.MarvelCharacterDetail
import io.reactivex.disposables.CompositeDisposable

class CharactersDatasourceFactory(private val compositeDisposable: CompositeDisposable,
                                  private val marvelRepository: MarvelRepository ): DataSource.Factory<Int, MarvelCharacterDetail>() {

    val characterDataSourceLiveData = MutableLiveData<CharacterDatasource>()

    override fun create(): DataSource<Int, MarvelCharacterDetail> {
        val characterDatasource = CharacterDatasource(marvelRepository, compositeDisposable)
        characterDataSourceLiveData.postValue(characterDatasource)
        return characterDatasource
    }
}