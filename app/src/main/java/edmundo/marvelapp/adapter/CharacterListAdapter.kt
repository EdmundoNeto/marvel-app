package edmundo.marvelapp.adapter

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.android.databinding.library.baseAdapters.BR
import edmundo.marvelapp.Model.MarvelCharacterDetail
import edmundo.marvelapp.R
import edmundo.marvelapp.databinding.CharacterListItemBinding
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject
import edmundo.marvelapp.Utils.State
import kotlinx.android.synthetic.main.item_list_footer.view.*


class CharacterListAdapter @Inject constructor(private var mMarverCharacterDetailList: MutableList<MarvelCharacterDetail>): PagedListAdapter<MarvelCharacterDetail, RecyclerView.ViewHolder>(MarvelCharacterDetailCallback) {

    private val mMarvelCharacterClickSubject = PublishSubject.create<MarvelCharacterDetail>()
    val mMarvelCharacterClickEvent: Observable<MarvelCharacterDetail> = mMarvelCharacterClickSubject

    private val DATA_VIEW_TYPE = 1
    private val FOOTER_VIEW_TYPE = 2

    private var state = State.LOADING

    companion object {
        val MarvelCharacterDetailCallback = object : DiffUtil.ItemCallback<MarvelCharacterDetail>() {
            override fun areItemsTheSame(oldItem: MarvelCharacterDetail, newItem: MarvelCharacterDetail): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: MarvelCharacterDetail, newItem: MarvelCharacterDetail): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasFooter()) 1 else 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == DATA_VIEW_TYPE)
            (holder as? MarvelCharacterViewHolder)?.bind(getItem(position)!!)
        else (holder as ListFooterViewHolder).bind(state)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemMarvelListAdapterBinding = CharacterListItemBinding.inflate(LayoutInflater.from(parent.context),
                parent, false)
        return if (viewType == DATA_VIEW_TYPE) MarvelCharacterViewHolder(itemMarvelListAdapterBinding)
                else ListFooterViewHolder.create(parent)
    }

    fun addItems(marvelCharacterDetailList: List<MarvelCharacterDetail>) {
        mMarverCharacterDetailList.addAll(marvelCharacterDetailList)
        notifyDataSetChanged()
    }

    fun clearItems() {
        mMarverCharacterDetailList.clear()
    }

    fun setState(state: State) {
        this.state = state
        notifyItemChanged(super.getItemCount())
    }

    override fun getItemViewType(position: Int): Int {
        return if (position < super.getItemCount()) DATA_VIEW_TYPE else FOOTER_VIEW_TYPE
    }

    private fun hasFooter(): Boolean {
        return super.getItemCount() != 0 && (state == State.LOADING || state == State.ERROR)
    }

    inner class MarvelCharacterViewHolder constructor(private val mBinding: CharacterListItemBinding)
        : RecyclerView.ViewHolder(mBinding.root) {


        init {
            mBinding.imageView.setOnClickListener {
                mMarvelCharacterClickSubject.onNext(mBinding.marvelCharacterDetail!!)
            }
        }

        fun bind(marvelCharacterDetail: MarvelCharacterDetail) {
            mBinding.setVariable(BR.marvelCharacterDetail, marvelCharacterDetail)
            mBinding.executePendingBindings()
        }

    }

    class ListFooterViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(status: State?) {
            itemView.progress_bar.visibility = if (status == State.LOADING) VISIBLE else View.INVISIBLE
            itemView.txt_error.visibility = if (status == State.ERROR) VISIBLE else View.INVISIBLE
        }

        companion object {
            fun create(parent: ViewGroup): ListFooterViewHolder {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_list_footer, parent, false)

                return ListFooterViewHolder(view)
            }
        }
    }

}