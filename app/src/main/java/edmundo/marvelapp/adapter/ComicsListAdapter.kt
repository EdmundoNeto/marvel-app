package edmundo.marvelapp.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.android.databinding.library.baseAdapters.BR
import edmundo.marvelapp.Model.MarvelComics
import edmundo.marvelapp.databinding.TabComicsCharacterDetailItemBinding
import javax.inject.Inject

class ComicsListAdapter @Inject constructor(private var marvelComics: MutableList<MarvelComics>): RecyclerView.Adapter<ComicsListAdapter.MarvelComicsViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): MarvelComicsViewHolder {
        val marvelComicsBinding = TabComicsCharacterDetailItemBinding.inflate(LayoutInflater.from(parent.context),
                parent, false)
        return MarvelComicsViewHolder(marvelComicsBinding)
    }

    override fun getItemCount(): Int {
        return marvelComics.size
    }

    override fun onBindViewHolder(holder: MarvelComicsViewHolder, position: Int) {
        (holder as? MarvelComicsViewHolder)?.bind(marvelComics[position])
    }


    fun addItems(comics: List<MarvelComics>) {
        marvelComics.addAll(comics)
        notifyDataSetChanged()
    }

    fun clearItems() {
        marvelComics.clear()
    }

    class MarvelComicsViewHolder constructor(private val mBinding: TabComicsCharacterDetailItemBinding)
        : RecyclerView.ViewHolder(mBinding.root) {


        fun bind(marvelComics: MarvelComics) {
            mBinding.setVariable(BR.marvelComics, marvelComics)
            mBinding.executePendingBindings()
        }

    }
}