package edmundo.marvelapp.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.android.databinding.library.baseAdapters.BR
import edmundo.marvelapp.Model.MarvelStories
import edmundo.marvelapp.databinding.TabStoriesCharacterDetailItemBinding
import javax.inject.Inject

class StoriesListAdapter @Inject constructor(private var marvelStories: MutableList<MarvelStories>): RecyclerView.Adapter<StoriesListAdapter.MarvelStoriesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): MarvelStoriesViewHolder {
        val marvelStoriesBinding = TabStoriesCharacterDetailItemBinding.inflate(LayoutInflater.from(parent.context),
                parent, false)
        return MarvelStoriesViewHolder(marvelStoriesBinding)
    }

    override fun getItemCount(): Int {
        return marvelStories.size
    }

    override fun onBindViewHolder(holder: MarvelStoriesViewHolder, position: Int) {
        (holder as? MarvelStoriesViewHolder)?.bind(marvelStories[position])
    }


    fun addItems(comics: List<MarvelStories>) {
        marvelStories.addAll(comics)
        notifyDataSetChanged()
    }

    fun clearItems() {
        marvelStories.clear()
    }

    class MarvelStoriesViewHolder constructor(private val mBinding: TabStoriesCharacterDetailItemBinding)
        : RecyclerView.ViewHolder(mBinding.root) {


        fun bind(marvelStories: MarvelStories) {
            mBinding.setVariable(BR.marvelStories, marvelStories)
            mBinding.executePendingBindings()
        }

    }
}