package edmundo.marvelapp.viewModel.factory

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import edmundo.marvelapp.viewModel.CharacterDetailViewModel
import javax.inject.Inject

class CharacterDetailViewModelFactory @Inject constructor(
        private val characterDetailViewModel: CharacterDetailViewModel) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CharacterDetailViewModel::class.java)) {
            return characterDetailViewModel as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}