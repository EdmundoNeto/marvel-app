package edmundo.marvelapp.viewModel.factory

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import edmundo.marvelapp.viewModel.CharacterListViewModel
import javax.inject.Inject

class CharacterListViewModelFactory  @Inject constructor(
        private val characterListViewModel: CharacterListViewModel) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CharacterListViewModel::class.java)) {
            return characterListViewModel as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}