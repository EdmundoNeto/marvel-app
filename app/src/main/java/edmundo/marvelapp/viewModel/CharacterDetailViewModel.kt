package edmundo.marvelapp.viewModel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableList
import edmundo.marvelapp.ApiService.MarvelRepository

import edmundo.marvelapp.Model.MarvelCharacterDetail
import edmundo.marvelapp.Model.MarvelComics
import edmundo.marvelapp.Model.MarvelSeries
import edmundo.marvelapp.Model.MarvelStories
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CharacterDetailViewModel @Inject constructor(private val marvelRepository: MarvelRepository): ViewModel() {

    var marvelRepositoryLoader: MutableLiveData<Boolean> = MutableLiveData()
    var marvelCharacterDetailResult: MutableLiveData<MarvelCharacterDetail> = MutableLiveData()
    var marvelCharacterDetailError: MutableLiveData<String> = MutableLiveData()

    lateinit var disposableObserver: DisposableObserver<MarvelCharacterDetail>

    val listMarvelComicsObservableArrayList: ObservableList<MarvelComics> = ObservableArrayList()
    val listMarvelSeriesObservableArrayList: ObservableList<MarvelSeries> = ObservableArrayList()
    val listMarvelStoriesObservableArrayList: ObservableList<MarvelStories> = ObservableArrayList()

    fun addSeriesToList(list: List<MarvelSeries>) {
        listMarvelSeriesObservableArrayList.clear()
        listMarvelSeriesObservableArrayList.addAll(list)
    }

    fun addStoriesToList(list: List<MarvelStories>) {
        listMarvelStoriesObservableArrayList.clear()
        listMarvelStoriesObservableArrayList.addAll(list)
    }

    fun addComicsToList(list: List<MarvelComics>) {
        listMarvelComicsObservableArrayList.clear()
        listMarvelComicsObservableArrayList.addAll(list)
    }

    fun marvelRepositoryLoader(): LiveData<Boolean> {
        return marvelRepositoryLoader
    }

    fun marvelCharacterDetailResult(): LiveData<MarvelCharacterDetail> {
        return marvelCharacterDetailResult
    }

    fun marvelCharacterDetailError(): LiveData<String> {
        return marvelCharacterDetailError
    }

    fun loadMarvelCharacterDetail(timeStamp: String, characterId: String, PUBLIC_KEY: String, hash: String) {

        disposableObserver = object : DisposableObserver<MarvelCharacterDetail>() {
            override fun onComplete() {
                marvelRepositoryLoader.postValue(false)
            }

            override fun onNext(resultsList: MarvelCharacterDetail) {
                marvelRepositoryLoader.postValue(true)
                marvelCharacterDetailResult.postValue(resultsList)
            }

            override fun onError(e: Throwable) {
                marvelCharacterDetailError.postValue(e.message)
                marvelRepositoryLoader.postValue(false)
            }
        }

        marvelRepository.getCharacterDetail(timeStamp, characterId, PUBLIC_KEY, hash)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(disposableObserver)
    }

}
