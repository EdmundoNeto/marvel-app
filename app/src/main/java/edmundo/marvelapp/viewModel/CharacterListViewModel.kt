package edmundo.marvelapp.viewModel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import android.databinding.ObservableArrayList
import android.databinding.ObservableList
import edmundo.marvelapp.ApiService.MarvelRepository
import edmundo.marvelapp.Model.MarvelCharacterDetail
import edmundo.marvelapp.Utils.MarvelUtils.LIMIT
import edmundo.marvelapp.adapter.CharacterDatasource
import edmundo.marvelapp.adapter.CharactersDatasourceFactory
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject
import edmundo.marvelapp.Utils.State

class CharacterListViewModel @Inject constructor(private val marvelRepository: MarvelRepository) : ViewModel(){

    val listMarvelCharactersObservableArrayList: ObservableList<MarvelCharacterDetail> = ObservableArrayList()

    var charactersList: LiveData<PagedList<MarvelCharacterDetail>>
    private val compositeDisposable = CompositeDisposable()
    private val charactersDatasourceFactory = CharactersDatasourceFactory(compositeDisposable, marvelRepository)

    init {
        val config = PagedList.Config.Builder()
                .setPageSize(LIMIT)
                .setInitialLoadSizeHint(LIMIT * 2)
                .setEnablePlaceholders(false)
                .build()
        charactersList = LivePagedListBuilder<Int, MarvelCharacterDetail>(charactersDatasourceFactory, config).build()
    }

    fun getState(): LiveData<State> = Transformations.switchMap<CharacterDatasource,
            State>(charactersDatasourceFactory.characterDataSourceLiveData, CharacterDatasource::state)

    fun retry() {
        charactersDatasourceFactory.characterDataSourceLiveData.value?.retry()
    }

    fun listIsEmpty(): Boolean {
        return charactersList.value?.isEmpty() ?: true
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}