package edmundo.marvelapp

import android.app.Activity
import android.app.Application
import android.provider.SyncStateContract
import android.support.v4.app.Fragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import edmundo.marvelapp.Utils.Constants
import edmundo.marvelapp.di.Component.DaggerAppComponent
import edmundo.marvelapp.di.Module.AppModule
import edmundo.marvelapp.di.Module.NetworkModule
import javax.inject.Inject

class MarvelApplication  : Application(), HasActivityInjector, HasSupportFragmentInjector{

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>
    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .networkModule(NetworkModule(Constants.API_BASE_URL))
                .build()
                .inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> = activityInjector
    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentInjector
}