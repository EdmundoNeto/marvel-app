package edmundo.marvelapp;

import org.junit.Test;

import edmundo.marvelapp.Utils.MarvelUtils;

import static junit.framework.Assert.assertEquals;

public class HashCalculatorTest {

    @Test
    public void calculateHash() {

        assertEquals("da4284fe0b2fc39431d1266c7eec0439",
                MarvelUtils.getHash(MarvelUtils.TIMESTAMP, MarvelUtils.PRIVATE_KEY, MarvelUtils.PUBLIC_KEY));
    }

}
