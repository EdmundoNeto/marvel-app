package edmundo.marvelapp;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.IOException;

import edmundo.marvelapp.ApiService.MarvelClient;
import edmundo.marvelapp.Model.Marvel;
import edmundo.marvelapp.Model.MarvelCharacters;
import edmundo.marvelapp.Utils.MarvelUtils;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;

public class ApiServiceTest {

    private MarvelClient apiService;
    private MockWebServer mockWebServer;

//    @Before
//    public void setUp() {
//        MockitoAnnotations.initMocks(this);
//        apiService = ServiceGenerator.createService(MarvelClient.class);
//    }

    @Test
    public void testApiResponse() {
        MarvelClient marvelClient = Mockito.mock(MarvelClient.class);

        final int limit = 20;
        final String timeStamp = MarvelUtils.TIMESTAMP;
        final String apiKey = "12345";
        final String hash = MarvelUtils.getHash(MarvelUtils.TIMESTAMP, MarvelUtils.PRIVATE_KEY, MarvelUtils.PUBLIC_KEY);


        final Call<Marvel<MarvelCharacters>> mockedCall = Mockito.mock(Call.class);

        Mockito.when(marvelClient.getCharacters(timeStamp, limit, apiKey, hash)).thenReturn(mockedCall);

        Mockito.doAnswer(new Answer<Marvel<MarvelCharacters>>() {
            @Override
            public Marvel<MarvelCharacters> answer(InvocationOnMock invocation) {
                Callback callback = (Callback) invocation.getArguments()[0];

                callback.onResponse(mockedCall, Response.success(new Marvel<MarvelCharacters>()));

                return null;
            }
        }).when(mockedCall).enqueue(any(Callback.class));

    }

    /**
     * Test that the API service can get a single comic resource.
     */


    @After
    public void tearDown() throws IOException {
        mockWebServer.shutdown();
    }

}