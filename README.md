# marvel-app


An android application that lists the Marvel characters. When entering the detail of a specific character the screen will show the description of the character and his participation in Comics, series and Stories

SplashScreen
![Splash Screen](https://gitlab.com/EdmundoNeto/marvel-app/blob/master/image1.png)

HomeScreen
![Home Screen](https://gitlab.com/EdmundoNeto/marvel-app/blob/master/image2.png)

CharacterDetailScreen
![CharacterDetailScreen](https://gitlab.com/EdmundoNeto/marvel-app/blob/master/image3.png)

CharacterDetailScreen 2
![CharacterDetailScreen2](https://gitlab.com/EdmundoNeto/marvel-app/blob/master/image4.png)

# Architecture
This application uses the Mode-View-ViewModel (popularly known as MVVM) architecture. 

